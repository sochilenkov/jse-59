package ru.t1.sochilenkov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class UserLoginResponse extends AbstractResultResponse {

    @Getter
    @Setter
    @Nullable
    private String token;

    public UserLoginResponse(@NotNull final String token) {
        this.token = token;
    }

    public UserLoginResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
