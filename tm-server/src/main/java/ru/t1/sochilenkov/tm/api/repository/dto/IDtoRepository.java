package ru.t1.sochilenkov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sochilenkov.tm.dto.model.AbstractModelDTO;
import ru.t1.sochilenkov.tm.enumerated.Sort;

import java.util.List;

public interface IDtoRepository<M extends AbstractModelDTO> {

    void add(@NotNull M model);

    void clear();

    boolean existsById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable final Sort sort);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    int getSize();

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void update(@NotNull M model);

}
